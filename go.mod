module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.1.6
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613
	golang.org/x/sys v0.0.0-20190204203706-41f3e6584952 // indirect
)
